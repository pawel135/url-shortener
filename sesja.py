# -*- coding: utf-8 -*-
from flask import Flask, session
from flask import request
from flask import render_template

app_url = '/szczepp3/sesja'
app = Flask(__name__)
app.secret_key = 'hfui3og43y9y3ufhuif'
app.debug = True

def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return [b"Hello World"]


@app.route(app_url + '/visit')
def visit():
  if 'number_of_visits' in session:
    session['number_of_visits'] = session['number_of_visits'] + 1
  else:
    session['number_of_visits'] = 1
  return "Odwiedzin: %d" % session['number_of_visits']

@app.route(app_url + '/login', methods=['GET', 'POST'])
def login():
  return render_template('login.html');

@app.route(app_url + '/shorten', methods=['GET','POST'])
def shorten():
  if request.method == 'POST':
    if request.form['shorten_button'] == 'Shorten':
      return render_template('shorten_url.html');
  elif request.method == 'GET':
    return "Improper method used"
  else:
    return "Cannot perform"

@app.route(app_url + '/validate', methods=['GET','POST'])
def validate():
  #TODO:  username = request.form['login']
  username = "trialUsername"
  links = ['firstLink', 'secondLink']
  if request.method == 'POST':
    if request.form['submit_button'] == 'Submit':
      print(username);
      return render_template('shortened_urls.html', usernameP=username, linksP=links);
  elif request.method == 'GET':
    return "Improper method used"
  else:
    return "Cannot perform"
"""
app_url = '/szczepp3/login'
app = Flask(__name)

@app.route(app_url +


"""





