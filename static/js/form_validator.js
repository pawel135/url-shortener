var author = "Paweł Szczepanowski";

$(function(){
	initialize();
	addListeners();
	validate();
});

function initialize(){
        $('#validatorW3').attr('href', 'https://validator.w3.org/check?uri=' + window.location.href +'&output=json');
		$('form#signin input:first').focus();
}

function addListeners(){
	$('header').click( function(){
		$('#signin').slideToggle(1000);
		$('#form_header').fadeToggle(900);
	});

	addAnimationListeners();
}

function addAnimationListeners(){
	$('#signin .label').click( function(){
		$(this).hide().animate({
			opacity: 0.95,
			left: "+=10",
			height: [ "toggle", "swing" ]
			//				height: "toggle"
		}, {
			duration: 500,
			specialEasing: {
				width: "linear",
				height: "easeOutBounce"
			}
		}, function() {

		});
	});
}
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function validate(){
	addCustomMethods();
	validator = $('#signin').validate({
		ignore: ":hidden",
        onkeyup: //function(element){
			debounce( function(element){
					this.element(element);
				}, 250, false),
//		onkeyup: function(element) {
//			var element_id = $(element).attr('id');
//			if (this.settings.rules[element_id].onkeyup !== false) {
//				$.validator.defaults.onkeyup.apply(this, arguments);
//			}
//        },
		onfocusout: function(element) {
			this.element(element);
		},
	/*	onchange: function(element){
			this.element(element);
		},
	*/	rules:{
			login: {
				required: true,
				rangelength: [5,20],
				loginInProperFormat: true
//              onkeyup: false
            },
			password:{
				required: true,
				rangelength: [8,20]
			}
		}, // rules
		messages:{
			password: {
				rangelength: jQuery.validator.format("Password must have between {0} and {1} characters ")
			}
		},//messages
		errorReplacement: function(error,element){
			if(element.is(':radio') || element.is(':checkbox')){
				error.appendTo(element.parent());
			} else{
				error.insertAfter(element);
			}
		}, //errorReplacement
/*                submitHandler: function (form) {
			//var formData = new FormData();
                        $.ajax({
				type: "POST",
				url: "http://edi.iem.pw.edu.pl/bach/register/user/",
				data: form.serialize() , //formData, //$('#signin input[name!=photo]').serialize();,
                                processData: false,
                                contentType: false, // 'multipart/form-data',
                                success: function (response) {
	                                alert(response);
        /*				$(form).html("<div id='message'></div>");
					var message = document.createElement("p");
					var textNode = document.createTextNode("");
					$('#message').html("<h2>Your request is on the way!</h2>")
						.append("<p>someone</p>")
						.hide()
						.fadeIn(1500, function () {
							$('#message').append("<img id='checkmark' src='images/ok.png' />");
						});

						}
			});
			return false; // required to block normal submit since ajax is used
		}
*/	});
}

function addCustomMethods(){
	jQuery.validator.addMethod("greaterThanZero", function(value, element) {
		return this.optional(element) || (parseFloat(value) > 0);
	}, "Value must be greater than zero");
	jQuery.validator.addMethod("loginInProperFormat", function(value, element){
		var re = /^[a-zA-Z]{1}\w*$/i;
		return this.optional(element) || re.test(value);
	},"Login format improper");
}

function handleData(responseMsg, username) {
  responseMsg = JSON.parse(responseMsg);
  var isFree = !responseMsg[username];
  console.log(isFree);
  return isFree;
}


/*
function pause(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) {   }
}
*/
