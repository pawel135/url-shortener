var author = "Paweł Szczepanowski";

$(function(){
	initialize();
	addLinkList();
	addListeners();
	validate();
});

function initialize(){
        $('#validatorW3').attr('href', 'https://validator.w3.org/check?uri=' + window.location.href +'&output=json');
}

function addLinkList(){
//	$('.main').prepend("<p>"+ links + "</p>"); //TODO:
	$('<div/>', {
    id: 'list_header',
    text: 'URL list for user ' + username
}).prependTo('.main');
}

function addListeners(){
	$('header').click( function(){
		$('.main').slideToggle(1000);
		$('#list_header').fadeToggle(900);
		//$('#signin').slideToggle(1000);
		//$('#form_header').fadeToggle(900);
	});

	addAnimationListeners();
}

function addAnimationListeners(){
/*	$('#signin .label').click( function(){
		$(this).hide().animate({
			opacity: 0.95,
			left: "+=10",
			height: [ "toggle", "swing" ]
			//				height: "toggle"
		}, {
			duration: 500,
			specialEasing: {
				width: "linear",
				height: "easeOutBounce"
			}
		}, function() {

		});
	});
*/
}

function pause(milliseconds) {
  var dt = new Date();
  while ((new Date()) - dt <= milliseconds) {   }
}
